import React, { Component } from 'react';
import AnimatedWrapper from './AnimatedWrapper.hoc';

class SubpageComponent extends Component {
  render() {
    return (
      <div className="page">
        <h1>Subpage</h1>
        <p>How's it hanging?</p>
      </div>
    );
  }
}

const Subpage = AnimatedWrapper(SubpageComponent);
export default Subpage;
