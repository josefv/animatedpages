import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import TransitionGroup from 'react-transition-group/TransitionGroup';

import Home from './home';
import Subpage from './subpage';

const firstChild = props => {
  const childrenArray = React.Children.toArray(props.children);
  return childrenArray[0] || null;
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="topMenu">
          <Link to="/">Home</Link>
          <Link to="/subpage">Subpage</Link>
        </div>
        <Switch>
          <Route
            exact
            path="/"
            children={({ match, ...rest }) => (
              <TransitionGroup component={firstChild}>{match && <Home {...rest} />}</TransitionGroup>
            )}
          />
          <Route
            path="/subpage"
            children={({ match, ...rest }) => (
              <TransitionGroup component={firstChild}>{match && <Subpage {...rest} />}</TransitionGroup>
            )}
          />
        </Switch>
      </div>
    );
  }
}

export default App;
