import React, { Component } from 'react';
import AnimatedWrapper from './AnimatedWrapper.hoc';

class HomeComponent extends Component {
  render() {
    return (
      <div className="page">
        <h1>Home Page</h1>
        <p>Hey there!</p>
      </div>
    );
  }
}

const Home = AnimatedWrapper(HomeComponent);
export default Home;
